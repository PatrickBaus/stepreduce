# STEP Reduce

This is a straightforward implementation of a lossless, one-way compression redundancy reduction algorithm targeted at STEP files.

STEP files are self-referential by nature.  When generating a file, the originating library can either generate a first-principles 
element for each structure or it can keep common elements singular and repeat.

In the case where the library generates first-principles elements for each structure, the file size can be extremely large.  And the
differences in referencing between common elements means that standard text compression such as gzip does not find the common elements.

STEP Reduce addresses this by removing duplicate elements from the step file and storing only the STEP reference instead.  In this manner,
the file is compressed in actual size without reducing the total informational content or format of the STEP file itself.

